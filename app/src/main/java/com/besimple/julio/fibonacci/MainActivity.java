package com.besimple.julio.fibonacci;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public int parada;
    public int valor1;
    public int valor2;
    public int fim;
    public String resultado = "";
    TextView fibonacci;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fibonacci = (TextView) findViewById(R.id.fibonacci);
        calcular(20);
    }


    public void calcular (int posicao) {

        parada = posicao;
        valor1 = 0;
        valor2 = 1;
        fim = 0;
        fim = valor2;

        resultado = String.valueOf(fim);
        parada--;

        while (parada >= 1){
            fim = valor1 + valor2;
            valor1 = valor2;
            valor2 =  fim;
            resultado = resultado + ", " + String.valueOf(fim);
            parada--;
        }

        fibonacci.setText(resultado);
    }
}
